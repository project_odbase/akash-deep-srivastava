<?php
/* Template Name: Home */

get_header();

?>

          <div class="wrap-body">
 	         <div class="col-lg-6">
					<!-- Content Box 01 -->
					<section class="box-content box-2 box-style-0" id="box-featured" data-anchor="featured">
						<div class="flex-box no-gutter">
							<div class="col-sm-8 f-right">
								<div id="owl-slide" class="owl-carousel">
									<?php $args = array('post_type' => 'sliders',
    'posts_per_page'                         => 6,

);
$wp_query = new WP_Query($args);

while (have_posts()): the_post();
    if (has_post_thumbnail()):
        $image_array = wp_get_attachment_image_src(get_post_thumbnail_id(), 'optional-size');
        $post_image  = $image_array[0];
    else:
        $post_image = home_url() . '/wp-content/themes/rcc/images/banner.jpg';
    endif;?>
										<div class="item">
											<div class="box-image">
												<img src="<?php echo $post_image; ?>" alt="" />
											</div>
										</div>
										<?php endwhile;?>

								</div>
							</div>
							<div class="col-sm-4">
								<div class="box-text">
									<div class="box-text-inner pd-medium-lg pd-medium-md pd-small-xs t-right">
										<div class="heading">
											<h2 class="small"><?php the_title();?></h2>
										</div>
										<div class="owl-controls ">
											<div class="btn next next-b_2-slide"><i class="fa fa-arrow-left"></i></div>
											<div class="btn prev next-b_2-slide"><i class="fa fa-arrow-right"></i></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>



			<!-- Content Box 02 -->
	          <div class="col-lg-6">
					<section class="box-content box-3 box-style-0">
						<div class="flex-box no-gutter">
							<div class="col-sm-4">
								<div class="box-image">
	<img src="<?php $src = wp_get_attachment_image_src(get_post_thumbnail_id(2), 'thumbnail_size');
$url                  = $src[0];
echo $url;?>" alt="" />
								</div>
							</div>
							<div class="col-sm-8">
								<div class="box-text ">
									<div class="box-text-inner pd-large-lg pd-small-xs">
									   <?php
$id   = 2;
$post = get_post($id);?>
			                           <div class="heading">
											 <h2><?php echo $post->post_title; ?></h2>
										</div>

					                   <p><?php echo $post->post_content; ?></p>
										<a href="<?php get_permalink();?>" class="btn btn-skin m-top30 new">Learn More</a>
									</div>
								</div>
							</div>
						</div>
					</section>
	            </div>

				<!-- Content Box 03 -->
	<div class="col-lg-6">
					<section class="box-content box-4">
						<div class="no-gutter">
							<?php
$args = array('post_type' => 'Projects',
    'posts_per_page'          => 8,
);
$loop = new WP_Query($args);
while ($loop->have_posts()): $loop->the_post();
    if (has_post_thumbnail()):
        $image_array = wp_get_attachment_image_src(get_post_thumbnail_id(), 'optional-size');
        $image       = $image_array[0];
    else:
        $image = get_template_directory_uri() . '/images/news-img1.jpg';
    endif;
    ?>
								<div class="col-lg-3 col-sm-6">
									<div class="portfolio-box zoom-effect">
										<img src="<?php echo $image; ?>" class="img-responsive" alt="">
										<div class="portfolio-box-caption">
											<div class="portfolio-box-caption-content">
												<div class="project-name">
												<a href="<?php the_permalink();?>"><?php the_title();?></a>
												</div>
												<div class="project-category">
													Category
												</div>
												<div class="project-social">
													<ul class="list-inline">
														<li><a href="<?php the_permalink();?>"><i class="fa fa-link"></i></a></li>
														<li><a href="images/10.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><i class="fa fa-search"></i></a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php endwhile;?>
						</div>
					</section>
				</div>

				<!-- Content Box 04 -->
				<div class="col-lg-6">
					<section class="box-content box-5 box-style-0">
						<div class="flex-box no-gutter">
							<div class="col-md-7">
								<div class="box-text dark-style">
									<div class="box-text-inner pd-medium-md pd-small-xs t-right">
										<?php
$id   = 47;
$post = get_post($id);
?>
										<div class="heading">

						<h2 style="font-size: 43px; line-height: 40px"><?php echo $post->post_title; ?></h2>
										</div>
										<h4 class="h5"><?php echo the_field('html'); ?></h4>
										<div class="progress">
											<div class="progress-bar progress-bar-yellow" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width:90%">
											  90%
											</div>
										</div>
										<h4 class="h5"><?php echo the_field('css3'); ?></h4>
										<div class="progress">
											<div class="progress-bar progress-bar-light" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width:95%">
											  95%
											</div>
										</div>
										<h4 class="h5"><?php echo the_field('jquery/javascript'); ?></h4>
										<div class="progress">
											<div class="progress-bar progress-bar-yellow" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%">
											  80%
											</div>
										</div>
										<h4 class="h5"><?php echo the_field('adobe_photoshop'); ?></h4>
										<div class="progress">
											<div class="progress-bar progress-bar-light" role="progressbar" aria-valuenow="88" aria-valuemin="0" aria-valuemax="100" style="width:88%">
											  88%
											</div>
										</div>
										<h4 class="h5"><?php echo the_field('adobe_dreamweaver'); ?></h4>
										<div class="progress">
											<div class="progress-bar progress-bar-yellow" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width:95%">
											  95%
											</div>
										</div>
										<ul class="list-inline social-link m-top20">
											<li><a href="<?php echo the_field('facebook'); ?>"><i class="fa fa-facebook"></i></a></li>
											<li><a href="<?php echo the_field('twitter'); ?>"><i class="fa fa-twitter"></i></a></li>
											<li><a href="<?php echo the_field('instagram'); ?>"><i class="fa fa-instagram"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-5">
								<div class="box-image">
                             <img src="<?php $src = wp_get_attachment_image_src(get_post_thumbnail_id(47), 'thumbnail_size');
$url                                              = $src[0];
echo $url;?>" alt="" />
								</div>
							</div>
						</div>
					</section>
				</div>

				<!-- Content Box Contact info -->
				<div class="col-md-6">
					<section class="box-content box-contact-info">
						<div class="flex-box no-gutter">
							<div class="col-sm-12">
								<div class="contact-info">
									<?php

$location = get_field('location');
if (!empty($location)):
?>
                    <div class="acf-map">
	                <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                    </div>
                    <?php endif;?>
									<!-- Show Info Button -->
									<a href="#" class="show-info-link"><i class="fa fa-info"></i> Show info</a>
									<div id="map" style="height: 550px;"></div>
									<address class="contact-info-wrapper pd-exlarge-sm pd-large-xs">
										<ul class="list-unstyled">
											<!-- Address -->
											<li class="contact-group">
											  <span class="adr-heading">Address</span>
											  <span class="adr-info">100 Street, Il, US</span>
											</li>
											<!-- Email -->
											<li class="contact-group">
											  <span class="adr-heading">Email</span>
											  <span class="adr-info">sayhello@email.com</span>
											</li>
										</ul>
										<ul class="list-unstyled">
											<!-- Phone -->
											<li class="contact-group">
											  <span class="adr-heading">Phone</span>
											  <span class="adr-info">+ 123 4567 890</span>
											</li>
											<!-- Mobile -->
											<li class="contact-group">
											  <span class="adr-heading">Mobile</span>
											  <span class="adr-info">+ 123 4567 890</span>
											</li>
										</ul>
										<a href="#" class="show-map"><i class="fa fa-map-o"></i> Show on map</a>
									</address>
								</div>
							</div>
						</div>
					</section>
				</div>

				<!-- Content Contact Form -06-->
				<div class="col-md-6">
					<section class="box-content box-contact-form box-style-0">
						<div class="flex-box no-gutter">
							<div class="col-sm-12">
								<div class="box-text">
									<div class="box-text-inner pd-exlarge-lg pd-small-xs">
										<div class="heading">
											<h2>Contact Us</h2>
										</div>
										<div>
											<!-- Name -->
											 <?php echo do_shortcode('[contact-form-7 id="8" title="Contact form 1"]'); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>



		<!-- End Content -->
		 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnj5z3GLq-XlUG4fla8xC_hrmmYCR_hMM&callback=initMap" async defer></script>
		 <script>
		$(document).ready(function() {
		  var b2_box = $("#owl-slide");

		  b2_box.owlCarousel({
			autoPlay: false,
			items : 1,
			itemsDesktop : [1199,1],
			itemsDesktopSmall : [979,1],
			itemsTablet : [768, 1],
			itemsMobile : [479, 1],
			pagination: false
		  });

		  // Custom Navigation Events 1
		  $(".next-b_2-slide").click(function(){
			b2_box.trigger('owl.next');
		  });
		  $(".prev-b_2-slide").click(function(){
			b2_box.trigger('owl.prev');
		  });

		});
	</script>

		<head>
 <style>
/* clearfix */
.owl-carousel .owl-wrapper:after {
	content: ".";
	display: block;
	clear: both;
	visibility: hidden;
	line-height: 0;
	height: 0;
}
/* display none until init */
.owl-carousel{
	display: none;
	position: relative;
	width: 100%;
	-ms-touch-action: pan-y;
}
.owl-carousel .owl-wrapper{
	display: none;
	position: relative;
	-webkit-transform: translate3d(0px, 0px, 0px);
}
.owl-carousel .owl-wrapper-outer{
	overflow: hidden;
	position: relative;
	width: 100%;
}
.owl-carousel .owl-wrapper-outer.autoHeight{
	-webkit-transition: height 500ms ease-in-out;
	-moz-transition: height 500ms ease-in-out;
	-ms-transition: height 500ms ease-in-out;
	-o-transition: height 500ms ease-in-out;
	transition: height 500ms ease-in-out;
}

.owl-carousel .owl-item{
	float: left;
}
.owl-controls .owl-page,
.owl-controls .owl-buttons div{
	cursor: pointer;
}
.owl-controls {
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	-webkit-tap-highlight-color: rgba(0, 0, 0, 0);
}

/* mouse grab icon */
.grabbing {
    cursor:url(grabbing.png) 8 8, move;
}

/* fix */
.owl-carousel  .owl-wrapper,
.owl-carousel  .owl-item{
	-webkit-backface-visibility: hidden;
	-moz-backface-visibility:    hidden;
	-ms-backface-visibility:     hidden;
  -webkit-transform: translate3d(0,0,0);
  -moz-transform: translate3d(0,0,0);
  -ms-transform: translate3d(0,0,0);
}


 </style>
 <style type="text/css">

.acf-map {
	width: 100%;
	height: 400px;
	border: #ccc solid 1px;
	margin: 20px 0;
}

/* fixes potential theme css conflict */
.acf-map img {
   max-width: inherit !important;
}

</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnj5z3GLq-XlUG4fla8xC_hrmmYCR_hMM"></script>
<script type="text/javascript">
(function($) {

/*
*  new_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

function new_map( $el ) {

	// var
	var $markers = $el.find('.marker');


	// vars
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};


	// create map
	var map = new google.maps.Map( $el[0], args);


	// add a markers reference
	map.markers = [];


	// add markers
	$markers.each(function(){

    	add_marker( $(this), map );

	});


	// center map
	center_map( map );


	// return
	return map;

}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

}

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/
// global var
var map = null;

$(document).ready(function(){

	$('.acf-map').each(function(){

		// create map
		map = new_map( $(this) );

	});

});

})(jQuery);
</script>
 </head>


  <?php get_footer();?>
  </div>