<?php
/**
 * odbase functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package odbase
 */

if ( ! function_exists( 'odbase_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function odbase_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on odbase, use a find and replace
		 * to change 'odbase' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'odbase', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'odbase' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'odbase_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'odbase_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function odbase_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'odbase_content_width', 640 );
}
add_action( 'after_setup_theme', 'odbase_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function odbase_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'odbase' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'odbase' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'odbase_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function odbase_scripts() {
	wp_enqueue_script("jquery");
	wp_enqueue_style( 'odbase-style', get_stylesheet_uri() );
	wp_enqueue_style( 'odbase-css', get_template_directory_uri() . '/css/style.css');
	wp_enqueue_style( 'odbase-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style( 'odbase-fontawesome', get_template_directory_uri() . '/font-awesome/css/font-awesome.css');
wp_enqueue_style( 'odbase-fontawesome', get_template_directory_uri() . '/font-awesome/css/font-awesome.min.css');
    wp_enqueue_style( 'odbase-css', get_template_directory_uri() . '/owl-carousel/owl.theme.css');
    wp_enqueue_style( 'odbase-css', get_template_directory_uri() . '/owl-carousel/owl.transitions.css');
    wp_enqueue_style( 'odbase-css', get_template_directory_uri() . '/owl-carousel/owl.carousel.css');

	wp_enqueue_script( 'odbase-custom', get_template_directory_uri() . '/js/bootstrap.min.js', true );
	wp_enqueue_script( 'odbase-bootstrap-js', get_template_directory_uri() . '/js/arctext.min.js', true );
	wp_enqueue_script( 'odbase-customizer', get_template_directory_uri() . '/js/customizer.js', true );
    
    wp_enqueue_script( 'odbase-html5shiv', get_template_directory_uri() . '/js/html5shiv.js', true );
   wp_enqueue_script( 'odbase-jquery-3.2.1.min', get_template_directory_uri() . '/js/jquery-3.2.1.min.js', true );
    wp_enqueue_script( 'odbase-main', get_template_directory_uri() . '/js/main.js', true );
    wp_enqueue_script( 'odbase-respond', get_template_directory_uri() . '/js/respond.min.js', true );
    wp_enqueue_script( 'odbase-validator', get_template_directory_uri() . '/js/validator.min.js', true );
     wp_enqueue_script( 'odbase-owl-carousel', get_template_directory_uri() . '/owl-carousel/owl.carousel.js', true );
      wp_enqueue_script( 'odbase-owl-carousel', get_template_directory_uri() . '/owl-carousel/owl.carousel.min.js', true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'odbase_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


/*Custom Post type end*/
function cw_post_type_projects() { 
$supports = array(
'title', 
'editor', 
'author', 
'thumbnail', 
'excerpt', 
'custom-fields', 
'comments', 
'revisions', 
'post-formats', 
);
 
$labels = array(
'name' => _x('Projects', 'plural'),
'singular_name' => _x('Projects', 'singular'),
'menu_name' => _x('Projects', 'admin menu'),
'name_admin_bar' => _x('Projects', 'admin bar'),
'add_new' => _x('Add New', 'add new'),
'add_new_item' => __('Add New Projects'),
'new_item' => __('New Projects'),
'edit_item' => __('Edit Projects'),
'view_item' => __('View Projects'),
'all_items' => __('All Projects'),
'search_items' => __('Search Projects'),
'not_found' => __('No Projects found.'),
);
 
$args = array(
'supports' => $supports,
'labels' => $labels,
'public' => true,
'query_var' => true,
'rewrite' => array('slug' => 'Projects'),
'has_archive' => true,

);
register_post_type('Projects', $args);
}
add_action('init', 'cw_post_type_projects');


//create a custom taxonomy name
add_action( 'init', 'custom_taxonomy', 0 ); 
function custom_taxonomy() {
  $labels = array(
    'name' => _x( 'Types', 'taxonomy general name' ),
    'singular_name' => _x( 'Type', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Types' ),
    'all_items' => __( 'All Types' ),
    'parent_item' => __( 'Parent Type' ),
    'parent_item_colon' => __( 'Parent Type:' ),
    'edit_item' => __( 'Edit Type' ), 
    'update_item' => __( 'Update Type' ),
    'add_new_item' => __( 'Add New Type' ),
    'new_item_name' => __( 'New Type Name' ),
    'menu_name' => __( 'Types' ),
  ); 	
  register_taxonomy('types',array('projects'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'type' ),
  ));
}
 
/*Custom Post type end*/
/*Custom Post type end*/
function cw_post_type_sliders() { 
$supports = array(
'title', 
'editor', 
'author', 
'thumbnail', 
'excerpt', 
'custom-fields', 
'comments', 
'revisions', 
'post-formats', 
);
 
$labels = array(
'name' => _x('Sliders', 'plural'),
'singular_name' => _x('Sliders', 'singular'),
'menu_name' => _x('Sliders', 'admin menu'),
'name_admin_bar' => _x('Sliders', 'admin bar'),
'add_new' => _x('Add New', 'add new'),
'add_new_item' => __('Add New Sliders'),
'new_item' => __('New Sliders'),
'edit_item' => __('Edit Sliders'),
'view_item' => __('View Sliders'),
'all_items' => __('All Sliders'),
'search_items' => __('Search Sliders'),
'not_found' => __('No Sliders found.'),
);
 
$args = array(
'supports' => $supports,
'labels' => $labels,
'public' => true,
'query_var' => true,
'rewrite' => array('slug' => 'Sliders'),
'has_archive' => true,

);
register_post_type('Sliders', $args);
}
add_action('init', 'cw_post_type_sliders');

//////////////////////////footer////
register_sidebar(array(
'name' => 'Footer Widget 1',
'id'        => 'footer-1',
'description' => 'First footer widget area',
'before_widget' => '<div id="footer-widget1">',
'after_widget' => '</div>',
'before_title' => '<h4>',
'after_title' => '</h4>',
));

// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'procility' ),
			 'header-menu' => __( 'Header Menu' ),
			 'menu-2' => __( 'Secondary' ),
		) );
		function my_acf_google_map_api( $api ){
	
	$api['key'] = 'AIzaSyCnj5z3GLq-XlUG4fla8xC_hrmmYCR_hMM';
	
	return $api;
	
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');