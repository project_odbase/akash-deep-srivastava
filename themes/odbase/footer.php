<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package odbase
 */

?>

	<footer id="page-footer" class="">
			<div class="bottom-footer">
				<div class="container-fluid">
					<div class="">
						<div class="col-md-6">
							<p>Copyright 20xx - Designed by <a href="https://www.html5xcss3.com">HTML5xCSS3</a></p>
						</div>
						<div class="col-md-6">
							<ul class="list-inline quicklinks text-right">
								<li><a href="#">Privacy Policy</a></li>
								<li><a href="#">Terms of Use</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer><!-- Footer -->

<?php wp_footer(); ?>

</body>
</html>
