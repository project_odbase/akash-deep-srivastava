<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package odbase
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body class="index-page">
	<div class="wrap-body">
		
		<!-- Content -->
		<div id="page-content" class="container-fluid">
			<div class="wrap-content">
				
				<!-- ////////////Content Box Header -->
				<div class="col-lg-6">
					<section class="box-content box-header">
						<!--  Header -->
						<div class="header-text">
							<div class="logo visible-xs text-center">
								<a href="<?php echo home_url(); ?>"><?php  
					  $custom_logo_id = get_theme_mod( 'custom_logo' );
           $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
            if ( has_custom_logo() ) {
              echo '<img class="img-responsive" src="'. esc_url( $logo[0] ) .'">';
            } else {
            echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
             } ?></a>
							</div>
							<div class="header-text-inner">
								<h1><?php echo the_field('logo_text'); ?></h1>
								
							</div>
						</div><!-- /header-text -->
					</section>
				  </div>
				  <div class="col-lg-6">
					<section class="box-content box-1 box-style-0">
						<div class="no-gutter">
							<div class="col-xs-4 social-box box-facebook">
								<a class="" href="<?php echo the_field('facebook'); ?>">
									<div class="social-box-inner">
										<div class="social-box-inner-content">
											<i class="fa fa-facebook"></i>
										</div>
									</div>
								</a>
							</div>
							<div class="col-xs-4 social-box box-twitter">
								<a class="" href="<?php echo the_field('twitter'); ?>">
									<div class="social-box-inner">
										<div class="social-box-inner-content">
											<i class="fa fa-twitter"></i>
										</div>
									</div>
								</a>
							</div>
							<div class="col-xs-4 social-box box-pinterest">
								<a class="" href="<?php echo the_field('pinterest'); ?>">
									<div class="social-box-inner">
										<div class="social-box-inner-content">
											<i class="fa fa-pinterest"></i>
										</div>
									</div>
								</a>
							</div>
							<div class="col-xs-4 social-box box-google">
								<a class="" href="<?php echo the_field('google_plus'); ?>">
									<div class="social-box-inner">
										<div class="social-box-inner-content">
											<i class="fa fa-google-plus"></i>
										</div>
									</div>
								</a>
							</div>
							<div class="col-xs-4 social-box box-instagram">
								<a class="" href="<?php echo the_field('instagram'); ?>">
									<div class="social-box-inner">
										<div class="social-box-inner-content">
											<i class="fa fa-instagram"></i>
										</div>
									</div>
								</a>
							</div>
							<div class="col-xs-4 social-box box-dribbble">
								<a class="" href="<?php echo the_field('dribble'); ?>">
									<div class="social-box-inner">
										<div class="social-box-inner-content">
											<i class="fa fa-dribbble"></i>
										</div>
									</div>
								</a>
							</div>
						</div>
					</section>
				</div>
				 </div>
				</div>
			</div>
</body>
</html>